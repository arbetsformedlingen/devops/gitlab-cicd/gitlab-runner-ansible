# Ansible Installer for Gitlab Runner

The purpose of this program is to setup a [Gitlab
Runner](https://docs.gitlab.com/runner/) service on a remote
Debian/Ubuntu server, using [Ansible](https://www.ansible.com/).


## Requirements
- Clone this repo
- Install [bws](https://github.com/bitwarden/sdk/releases)
- Install [bw](https://bitwarden.com/help/cli/)
- Install [Ansible](https://www.ansible.com/)
- Install make, python and python-venv
- Install library containing Bitwarden integration: `ansible-galaxy collection install community.general`


## Prepare Vaultwarden och Bitwarden

1. In your vault, create a new organisation, and a collection in that
organisation. They are refered to as ORG and COLL below.

2. Create a gitlab runner in Gitlab. This will give you a name and a secret token.

3. Create the following secrets in that ORG and COLL:

| name | type | desc |
|---|---|---|
| af_proxy | note | the proxy url, if needed |
| dockerhub_auth | note | dockerhub authentication key |
| dockerhub_login | login | dockerhub login username and password |
| runner_token_onprem2 | login | gitlab runner name and secret token (as created above) |

4. Create the file `Makefile.settings`:
```
VW_HOST        = https://vaultwarden-test.jobtechdev.se
VW_USER        = <your vaultwarden username>
ORG            = <the ORG name you created>
COLLECTION     = <the COLL name you created in ORG>
PYTHON_VERSION = python3.11
HOSTS          = hosts.yaml
```

5. Edit `playbook.yml`:
 - set `configure_proxy` to `true` or `false` (without quotes)
 - edit `runner_token_username` and `runner_token_password`:  make sure the lookup rules refer to the runner secret you want to use (by default `runner_token_onprem2`).


6. Create a `hosts.yml` (or any other name):
```
runner:
  hosts:
    <external IP of the host>
  vars:
    ansible_user: "<ssh user>"
    ansible_ssh_private_key_file: "<ssh private key>"
```

7. Validate the connection with `ansible runner -m ping -i hosts.yml`

## Prepare Docker Daemon
Make sure it is configured for [TCP access](https://docs.docker.com/engine/daemon/remote-access/):
```
$ sudo netstat -lntp | grep dockerd
tcp        0      0 127.0.0.1:2375          0.0.0.0:*               LISTEN      3758/dockerd
```
Otherwise edit `/etc/systemd/system/multi-user.target.wants/docker.service` and set
```
ExecStart=/usr/bin/dockerd -H fd:// -H tcp://127.0.0.1:2375
```
then
```
sudo systemctl daemon-reload
sudo systemctl restart docker.service
```

## Usage

`make run`


## Development workflow
Create a new Gitlab Runner token in a test project in Gitlab (on the
creation page, configure it to run untagged jobs), store the token in
Vaultwarden, and configure [playbook.yml](playbook.yml) to use it. The
test project should have a `gitlab-ci.yml` file so you can run a
pipeline in the runner. Make sure to turn off the community runners in
Settings -> CI/CD -> Runners, and make sure your own runner is
activated.

After installing the Ansible playbook on the server, you can monitor the log
with `journalctl -u gitlab-runner -f`. You could also turn off the service
with `sudo systemctl stop gitlab-runner`, and then run it in the foreground
with `sudo gitlab-runner --debug run`.

It's best to make changes locally and deploy with Ansible, so you can
easier track your changes.

When you have deployed a change, simply start a new pipeline run in
your gitlab project.



## Status

Here is a test result table. "Internet" means a regular server, like
AWS EC2.  "Arbetsförmedlingen's VMs behind proxy" means a VM run on
Red Hat Virtualization on Arbetsförmedlingens network.

|                       | Internet |  Arbetsförmedlingen's VMs behind proxy  |
|-----------------------|----------|-----------------------------------------|
| Regular jobs          | works    | works                                   |
| Docker-in-Docker jobs | works    | problem, possibly with shared volumes   |

The problem with DIND-jobs on Arbetsförmedlingens VMs: the job runs
without error, but is seems like any output files/artifacts produced
by the DIND-job does not end up in the environment which started the
job.


## TODO
Supposedly, Docker runs faster using an overlay storage driver, so
 - load module `overlay` (add to /etc/modules, do modprobe)
 - configure the required Docker environment variables


## Maintainers

The devops team at JobTech - see [CODEOWNERS](CODEOWNERS).



## Links
- Ansible bitwarden integration [community.general.bitwarden](https://docs.ansible.com/ansible/latest/collections/community/general/bitwarden_lookup.html)
