SHELL          = /bin/bash

include Makefile.settings


run: venv bw.secrets
	. venv/bin/activate \
	&& . bw.secrets \
	&& ansible-playbook -vv -i $(HOSTS) playbook.yml


dry-run: venv bw.secrets
	. venv/bin/activate \
	&& . bw.secrets \
	&& ansible-playbook --check --diff -vv -i $(HOSTS) playbook.yml


syntax-check: venv bw.secrets
	. venv/bin/activate \
	&& . bw.secrets \
	&& ansible-playbook --syntax-check -vv -i $(HOSTS) playbook.yml


.PHONY: bw.secrets
bw.secrets:
	bash make__bw_login.sh "$(VW_USER)" "$(VW_HOST)" "$(ORG)" "$(COLLECTION)" "$@"



venv/lib/$(PYTHON_VERSION)/site-packages/bitwarden_sdk/bitwarden_client.py: venv
	. venv/bin/activate \
	&& pip install bitwarden-sdk


venv:
	$(PYTHON_VERSION) -m venv venv
