#!/bin/bash


VW_USER="$1"
VW_HOST="$2"
ORG="$3"
COLLECTION="$4"
OUTPUT_FILE="${5:-bw.secrets}"


if [ ! -f "$OUTPUT_FILE" ] || ! . "$OUTPUT_FILE" || [ $(bw status --raw | jq -r .status) != "unlocked" ]; then
  if [ $(bw status --raw | jq -r .status) == "unauthenticated" ]; then
          bw config server "$VW_HOST"
          export BW_SESSION=$(bw login --method 0 --raw "$VW_USER")
  fi
  if [ $(bw status --raw | jq -r .status) == "locked" ]; then
          export BW_SESSION=$(bw unlock --raw)
  fi

  # Get ID of organisation
  ORG_ID=$(bw list organizations | jq -r '.[] | select(.name=="'$ORG'") | .id')

  ## Get ID of collection
  COLLECTION_ID=$(bw list collections | jq -r '.[] | select(.name=="'$COLLECTION'" and .organizationId=="'$ORG_ID'") | .id')

  echo "export BW_SESSION='$BW_SESSION'" >  "$OUTPUT_FILE"
  echo "export BWS_ACCESS_TOKEN='$BW_SESSION'" >> "$OUTPUT_FILE"
  echo "export COLLECTION_ID='$COLLECTION_ID'" >> "$OUTPUT_FILE"
  echo "export ORG_ID='$ORG_ID'" >> "$OUTPUT_FILE"
fi

. "$OUTPUT_FILE"
bw sync
